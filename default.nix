{ compiler ? "default", doBenchmark ? false }:

let
  rev = "dd98e07fed9abe82f43dec5497b7978d413266ff";

  pkgs = import
      (builtins.fetchTarball { url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";     }) {};


  f = { mkDerivation, base, bytestring, newtype-generics
      , profunctors, stdenv, text
      }:
      mkDerivation {
        pname = "control-iso";
        version = "0.1.0.0";
        src = ./.;
        libraryHaskellDepends = [
          base bytestring newtype-generics profunctors text
        ];
        description = "A typeclass for type isomorphisms";
        license = stdenv.lib.licenses.bsd3;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
